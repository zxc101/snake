// Fill out your copyright notice in the Description page of Project Settings.


#include "Energetic.h"
#include "SnakeBase.h"

// Sets default values
AEnergetic::AEnergetic()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnergetic::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEnergetic::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnergetic::Interact(AActor* Interactor, bool IsHead)
{
	if (IsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddMovementSpeed(RetarderSpeed);
			SetActorLocation(FVector(FMath::FRandRange(LimitPosX.X, LimitPosX.Y),
							 FMath::FRandRange(LimitPosY.X, LimitPosY.Y),
							 0));
		}
	}
}

