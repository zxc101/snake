// Fill out your copyright notice in the Description page of Project Settings.


#include "Tablet.h"
#include "SnakeBase.h"

// Sets default values
ATablet::ATablet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATablet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATablet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATablet::Interact(AActor* Interactor, bool IsHead)
{
	if (IsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->RemoveLastElemtnt();
			SetActorLocation(FVector(FMath::FRandRange(LimitPosX.X, LimitPosX.Y),
				FMath::FRandRange(LimitPosY.X, LimitPosY.Y),
				0));
		}
	}
}

