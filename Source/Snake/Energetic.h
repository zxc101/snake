// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Energetic.generated.h"

UCLASS()
class SNAKE_API AEnergetic : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnergetic();

	UPROPERTY(EditAnywhere)
	float RetarderSpeed;

	UPROPERTY(EditDefaultsOnly)
	FVector2D LimitPosX;

	UPROPERTY(EditDefaultsOnly)
	FVector2D LimitPosY;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool IsHead) override;

};
