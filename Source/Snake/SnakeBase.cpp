// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
	LimitMovementSpeed = FVector2D(0.1f, 1.f);
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int32 ElementsNum)
{
	for (int32 i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->MeshComponent->SetVisibility(false);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::RemoveLastElemtnt()
{
	SnakeElements[SnakeElements.Num() - 1]->Destroy();
	SnakeElements.RemoveAt(SnakeElements.Num() - 1);
}

void ASnakeBase::Remove()
{
	for (int32 i = SnakeElements.Num() - 1; i >= 0; i--)
	{
		SnakeElements[i]->Destroy();
	}
	SnakeElements.Empty();
	Destroy();
}

void ASnakeBase::AddMovementSpeed(float Speed)
{
	MovementSpeed += Speed;
	if (LimitMovementSpeed.X > MovementSpeed)
	{
		MovementSpeed = LimitMovementSpeed.X;
	}
	else if (LimitMovementSpeed.Y < MovementSpeed)
	{
		MovementSpeed = LimitMovementSpeed.Y;
	}
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::Move()
{
	FVector MovementVector = FVector(0.f, 0.f, 0.f);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	default:
		break;
	}

	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->MeshComponent->SetVisibility(true);

	for (int32 i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		SnakeElements[i]->MeshComponent->SetVisibility(true);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool IsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, IsFirst);
		}
	}
}